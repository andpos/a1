import java.util.Arrays;

public class Sheep {
   /*
   * Olgu massiivis juhuslikult läbisegi sikud (goat) ja lambad (sheep). Koostage võimalikult kiire meetod, mis järjestaks massiivi ümber nii, et kõik sikud oleksid massiivi alguses ja kõik lambad lõpus. Arvestage ka piirjuhtumiga, et kõik loomad on üht sorti.
   *
   * */

   enum Animal {sheep, goat};
   
   public static void main (String[] param) {
      // for debugging
      Animal[] animals = {Animal.sheep, Animal.sheep, Animal.goat, Animal.sheep, Animal.goat, Animal.sheep, Animal.sheep, Animal.goat, Animal.goat};
      reorder(animals);
      System.out.println(Arrays.toString(animals));
   }

   public static void reorder(Animal[] animals) {
      int goat = animals.length - 1;
      int sheep = 0;
      do {
         // find sheep from front
         for (; sheep < animals.length; ++sheep) {
            if (animals[sheep] == Animal.sheep) break;
         }
         // find goat from back
         for (; goat > -1; --goat) {
            if (animals[goat] == Animal.goat) break;
         }
         // break after crossing paths
         if (goat <= sheep) break;

         // swap positions
         Animal tmp = animals[goat];
         animals[goat--] = animals[sheep];
         animals[sheep++] = tmp;
      } while (true);
   }
}
